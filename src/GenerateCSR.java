import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;

import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.encoders.Hex;
import sun.security.pkcs.PKCS10;
import sun.security.x509.X500Name;
import sun.security.x509.X500Signer;

public class GenerateCSR {
    private static final String smsActivationCode = "12345678910";
    private static PublicKey publicKey = null;
    private static PrivateKey privateKey = null;
    private static KeyPairGenerator keyGen = null;
    private static GenerateCSR gcsr = null;

    private GenerateCSR() {
        try {
            keyGen = KeyPairGenerator.getInstance("DSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        keyGen.initialize(1024, new SecureRandom());
        KeyPair keypair = keyGen.generateKeyPair();
        publicKey = keypair.getPublic();
        privateKey = keypair.getPrivate();
    }

    public static GenerateCSR getInstance() {
        if (gcsr == null)
            gcsr = new GenerateCSR();
        return gcsr;
    }

    public String getCSR(String cn) throws Exception {
        byte[] csr = generatePKCS10(cn);
        return new String(csr);
    }

    private static byte[] generatePKCS10(String params) throws Exception {
        // generate PKCS10 certificate request
        String sigAlg = "SHA1withDSA";
        OutputStream out = new ByteArrayOutputStream();
        PKCS10 pkcs10 = new PKCS10(publicKey);
        Signature signature = Signature.getInstance(sigAlg);
        signature.initSign(privateKey);
        X500Name x500Name = new X500Name(params);
        pkcs10.encodeAndSign(new X500Signer(signature, x500Name));
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(bs);
        pkcs10.print(ps);
        byte[] c = bs.toByteArray();
        try {
            if (ps != null)
                ps.close();
            if (bs != null)
                bs.close();
        } catch (Throwable th) {
        }
        return c;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public static HMac perform(String message, String key)
    {
        HMac hmac = new HMac(new SHA1Digest());
        byte[] resBuf = new byte[hmac.getMacSize()];
        {
            byte[] m = message.getBytes();
            if (message.startsWith("0x"))
            {
                m = Hex.decode(message.substring(2));
            }
            hmac.init(new KeyParameter(Hex.decode(key)));
            hmac.update(m, 0, m.length);
            hmac.doFinal(resBuf, 0);

        }
        return hmac;
    }

    public static void main(String[] args) throws Exception {
        GenerateCSR gcsr = GenerateCSR.getInstance();

        System.out.println("Public Key:\n" + gcsr.getPublicKey().toString());

        System.out.println("Private Key:\n" + gcsr.getPrivateKey().toString());

        String csr = gcsr.getCSR("CN=some, serialNumber=test, C=ET");

        System.out.println("Hmac seal:\n" + perform(smsActivationCode, csr.toString()).getAlgorithmName());

        System.out.println("CSR Request Generated!!");
        System.out.println(csr);
    }

}
